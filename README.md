# Universidade Federal de Goiás
## Para o corretor:
* Para conferir o funcionamento, rode o app.js em um terminal e o shellscript.sh em outro.

## Exercício REST
**Complete o tutorial REST apresentado em sala de aula, implementando seu próprio exemplo, incluindo obrigatoriamente:**

**1) persistir as modificações no arquivo users.json**

**2) pelo menos mais uma pesquisa parametrizada, por id ou outra propriedade da pessoa**

**3) uma mesma funcionalidade, por exemplo "deletar pessoa", implementada no mesmo caminho, mas com variações de comando, por exemplo:**

**usando GET, com caminho /deleteUser/1**

**usando DELETE, com corpo body JSON: {"user": "1"}**

**usando POST, com corpo body JSON: {"user": "1"}**

