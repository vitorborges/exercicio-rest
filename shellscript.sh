#!/bin/bash

echo "Hello"
echo "Listing users."
curl -X GET -i http://localhost:8081/listUsers

echo "Get user with id:2"
curl -X GET -i http://localhost:8081/2

echo "Get mahesh"
curl -X GET -i http://localhost:8081/getUser/mahesh

echo "Create new user"
curl -X POST -H 'Content-Type: application/json' -i 'http://localhost:8081/addUser' --data '
{
	"name": "mohit",
	"password": "password4",
	"profession": "developer",
	"id": null
}'

echo "Delete new user"
curl -X DELETE -i 'http://localhost:8081/deleteUser/8'

echo "Create new user"
curl -X POST -H 'Content-Type: application/json' -i 'http://localhost:8081/addUser' --data '
{
	"name": "mohit",
	"password": "password4",
	"profession": "developer",
	"id": null
}'

echo "Delete new user"
curl -X DELETE -i 'http://localhost:8081/deleteUser/8'

echo "The end"
