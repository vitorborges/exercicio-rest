var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var fs = require("fs");

/* var user = {
	"user4" : {
		"name" : "mohit",
		"password" : "password4",
		"profession" : "teacher",
		"id": 4
	}
}
*/

app.use(bodyParser.json()); // for parsing application/json

app.get('/listUsers', function (req, res) {
	fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) {
		console.log( data );
		res.end( data );
	});
})

app.post('/addUser', function (req, res) {
	// First read existing users.
	fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) {
		console.log( req.body );
		console.log( data );
		var buffer = JSON.parse(data);
		buffer.push(req.body);
		buffer[buffer.length - 1].id = buffer.length;
		var return_value = JSON.stringify(buffer)
		fs.writeFile("users.json", return_value, 'utf8', function(err){
			if(err){ console.log(err); return -1; }
			res.end(return_value);
		});

	});
});

app.get('/:id', function (req, res) {
	// First read existing users.
	fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) {
		data = JSON.parse( data );
		var user = data[ req.params.id - 1] 
		console.log( user );
		res.end( JSON.stringify(user));
	});
});

app.delete('/deleteUser/:id', function (req, res) {
	// First read existing users.
	fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) {
		data = JSON.parse( data );
		data.splice(req.params.id - 1, 1);
		
		console.log( data );
		var return_value = JSON.stringify(data);
		fs.writeFile("users.json", return_value, 'utf8', function(err){
			if(err){ console.log(err); return -1; }
			res.end(return_value);
		});
	});
});

app.get('/deleteUser/:id', function (req, res) {
	// First read existing users.
	fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) {
		data = JSON.parse( data );
		data.splice(req.params.id - 1, 1);
		
		console.log( data );
		var return_value = JSON.stringify(data);
		fs.writeFile("users.json", return_value, 'utf8', function(err){
			if(err){ console.log(err); return -1; }
			res.end(return_value);
		});
	});
});

app.get('/getUser/:name', function(req, res){
	//First read existing users
	fs.readFile(__dirname + "/" + "users.json", "utf8", function(err, data){
		var buffer = JSON.parse(data);
		console.log(buffer.length);
		buffer.forEach(user =>{
			if(user.name === req.params.name){
				var response = user;
				console.log(response);
				res.end(JSON.stringify(response));
			}
		});
	});
});

var server = app.listen(8081, function () {
	var host = server.address().address
	var port = server.address().port

	console.log("Example app listening at http://%s:%s", host, port)
});

